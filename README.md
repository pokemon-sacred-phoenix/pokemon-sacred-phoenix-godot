# Pokémon Godot Starter Kit

French Starter Kit for making Pokémon Fangames in Godot. 

## Useful software

- Godot (needed) : https://godotengine.org/download

- Tiled (?) : https://www.mapeditor.org/download.html

- Libresprite (?) : https://libresprite.github.io/#downloads

## Useful plugins

- Dialogic ou Simple Dialog Manager pour les dialogues, Input Remapper pour la gestion des contrôles
