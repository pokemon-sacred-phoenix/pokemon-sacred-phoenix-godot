extends Node2D

var point1 = Vector2(640,380)
var point2 = Vector2(640,360)
var time = 0
var timeDirection = 1
var moveDuration = 1
var blink = 0

func _ready():
	$BGM_MainMenu.play()
	var screenSize = get_viewport().get_visible_rect().size
	point1 = Vector2(640, 360)
	point2 = Vector2(640, 320)

func _process(delta):
	if (time > moveDuration or time < 0):
		timeDirection *= 0
	time += delta * timeDirection
	var t = time / moveDuration

	$Soleil.position = lerp(point1,point2, t)
	if (time > moveDuration):
		$Titleblason.show()
		$Titlelogo.show()
		$BlinkMenu.show()
		$BlinkMenu.play()

			
	if Input.is_action_pressed("ui_accept"):
		get_tree().change_scene("res://Scenes/Titlescreen/Titlescreen.tscn")
